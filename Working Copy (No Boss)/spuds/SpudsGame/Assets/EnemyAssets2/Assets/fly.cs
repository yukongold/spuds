﻿using UnityEngine;
using System.Collections;

public class fly : MonoBehaviour {

	public Transform whoToFollow;
	public float velocity= 2f;
	public float distanceFromTargetX = 1.0f;
	public float distanceFromTargetY = 1.0f;

	public bool willItTurn = false;

	private Transform thisTransform;

	private bool facingRight = true;
	private float distance;

	void  Start (){
		thisTransform = transform;
	}


	void  LateUpdate (){

		float positionX;
		float positionY;

		if (willItTurn) {

			if (GameObject.Find (whoToFollow.name).GetComponent<Player_Move> ().facingRight) {
				positionX = Mathf.Lerp (thisTransform.position.x, whoToFollow.position.x - distanceFromTargetX, Time.deltaTime * velocity);
			} else {
				positionX = Mathf.Lerp (thisTransform.position.x, whoToFollow.position.x + distanceFromTargetX, Time.deltaTime * velocity);
			}
			positionY = Mathf.Lerp (thisTransform.position.y, whoToFollow.position.y + distanceFromTargetY, Time.deltaTime * velocity);

			thisTransform.position = new Vector3 (positionX, positionY, 0);

			if (GameObject.Find (whoToFollow.name).GetComponent<Player_Move> ().facingRight == true && !facingRight) {
				Flip ();
			} else if (GameObject.Find (whoToFollow.name).GetComponent<Player_Move> ().facingRight == false && facingRight) {
				Flip ();
			}
		} else if (!willItTurn) {

			positionX = Mathf.Lerp (thisTransform.position.x, whoToFollow.position.x - distanceFromTargetX, Time.deltaTime * velocity);
			positionY = Mathf.Lerp (thisTransform.position.y, whoToFollow.position.y + distanceFromTargetY, Time.deltaTime * velocity);
			thisTransform.position = new Vector3 (positionX, positionY, 0);

		}
	}

	void Flip(){
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

}