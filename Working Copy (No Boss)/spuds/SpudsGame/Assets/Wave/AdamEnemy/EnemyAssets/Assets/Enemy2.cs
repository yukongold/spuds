﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : MonoBehaviour {
	public int EnemySpeed;
	public int XMoveDirection;
	public int YMoveDirection;

	void Update(){
		RaycastHit2D hit = Physics2D.Raycast (transform.position, new Vector2 (XMoveDirection, 0));
		gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (XMoveDirection,0)* EnemySpeed;

		RaycastHit2D hit2 = Physics2D.Raycast (transform.position, new Vector2 (YMoveDirection, 0));
		gameObject.GetComponent<Rigidbody2D> ().velocity = new Vector2 (YMoveDirection,0)* EnemySpeed;
		if (hit.distance< 1f || hit2.distance <1f)  {
				Flip ();
		}
    }

	void Flip(){
		if(XMoveDirection >0 || YMoveDirection > 0){
			XMoveDirection = -1;
			YMoveDirection = -1;
		}
		else{
			XMoveDirection = 1;
			YMoveDirection = 1;
		}
    }
}