﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawner : MonoBehaviour {

    private bool canSpawn = true; // debug, enable/disable enemy spawn
    public enum SpawnState {SPAWNING, WAITING, COUNTING};
    private Game_Stats gameStats;

    [System.Serializable] //allows editing inside Unity
    public class Wave
    {
        public string name; //name of wave
        public Transform enemy; //point at which enemy spawns
        public int count; //amount spawned
        public float rate; 
    }

    public Wave[] waves;
    private int nextWave = 0; //index of next wave

    public Transform[] spawnPoints;

    public float timeBetweenWaves = 4f;
    public float waveCountdown;

    private float searchCountdown = 1f; 

    private SpawnState state = SpawnState.COUNTING;

	[SerializeField]
	private GameObject winScreenUI;

    void Start()
    {
        gameStats = GameObject.FindGameObjectWithTag("GameStats").GetComponent<Game_Stats>();
        if (spawnPoints.Length == 0)
        {
            Debug.LogError("No spawn points referenced");
        }

        waveCountdown = timeBetweenWaves;
    }

    void Update()
    {
        if (canSpawn)
        {
            if (state == SpawnState.WAITING)
            {
                if (!EnemyIsAlive())
                {
                    WaveCompleted();
                }
                else
                {
                    Debug.Log("still have enemies");
                    return;
                }
            }

            if (waveCountdown <= 0) //Countdown wave and spawn
            {
                if (state != SpawnState.SPAWNING)
                {
                    StartCoroutine(SpawnWave(waves[nextWave]));
                    //Enumerates through all waves to be spawned using IEnum
                }
            }
            else
            {
                waveCountdown -= Time.deltaTime;
            }
        }
    }

    void WaveCompleted()
    {
        //Begin new round
        Debug.Log("Wave completed");
        gameStats.wavesCleared++;
        state = SpawnState.COUNTING;
        waveCountdown = timeBetweenWaves;

        if(nextWave + 1 > waves.Length - 1)
        {
            nextWave = 0;
			winScreenUI.SetActive (true);
            gameStats.Opacity(true);
			Time.timeScale = 0;
            Debug.Log("All waves complete, Looping");
        } else {
            nextWave++;
        }   
    }

    bool EnemyIsAlive()
    {
        searchCountdown -= Time.deltaTime;
        if(searchCountdown <= 0f)
        {
            searchCountdown = 1f;
            if (GameObject.FindGameObjectWithTag("Enemy") == null) {
                return false;
            }
        }
        return true;
    } 

    IEnumerator SpawnWave (Wave _wave)
    {
        Debug.Log("New wave:" + _wave.name);
        state = SpawnState.SPAWNING; 
        
        for (int i = 0; i < _wave.count; i++)
        {
            SpawnEnemy(_wave.enemy);
            yield return new WaitForSeconds( 1f/_wave.rate); 
            //waits for 1 sec divided by rate of spawn
            //can also use _wave.delay for fixed delay
        }

        state = SpawnState.WAITING; 

        yield break;
    }

    void SpawnEnemy (Transform _enemy)
    {
        Debug.Log("Spawning Enemy: " + _enemy.name);

        Transform _sp = spawnPoints[ Random.Range(0, spawnPoints.Length) ];
        Instantiate(_enemy, _sp.position, _sp.rotation); 
    }
}
