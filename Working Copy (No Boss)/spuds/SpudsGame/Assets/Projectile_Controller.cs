﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile_Controller : MonoBehaviour {

    public float xSpeed;
    public float ySpeed;

    public GameObject impactEffect;
	
	//!!!NEEDS TO CHANGE FOR MULTIPLE ENEMIES!!!
	private FlyingFollower flyEnemy;
    private Game_Stats gameStats;

	// Use this for initialization
	void Start () {
        StartCoroutine(DestroyBullet());
		flyEnemy = GameObject.FindGameObjectWithTag("Enemy").GetComponent<FlyingFollower>();
        gameStats = GameObject.FindGameObjectWithTag("GameStats").GetComponent<Game_Stats>();
    }
	
	// Update is called once per frame
	void Update () {
        Vector2 position = transform.position;
        position.x += xSpeed;
        position.y += ySpeed;
        transform.position = position;
    }

    // Destroy projectile after certain time
    IEnumerator DestroyBullet()
    {
        yield return new WaitForSeconds(4f);
        Destroy(gameObject);
    }

    // Collision interaction
    private void OnTriggerEnter2D(Collider2D collision) // may be bugged, sometimes 1 hit registers as two triggers
    {
        // Hurt Enemy
        if(collision.gameObject.tag == "Enemy")
        {
            flyEnemy.Damage(1); 
            gameStats.shotsHit++; 
        }

        // Destroy particle effects after animation
        Destroy(Instantiate(impactEffect, transform.position, transform.rotation), 0.3f);

        // Destroy projectile on collision
        Destroy(gameObject);
    }
}
