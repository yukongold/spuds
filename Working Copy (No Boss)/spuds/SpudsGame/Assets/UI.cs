﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour {

	public Sprite[] heartSprites;
	public Image healthUI;
	private Player_Move player;
	
	void Start() {
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Move>();
	}
	
	void Update(){
		
		healthUI.sprite = heartSprites[player.currentHealth];
		
	}


}
