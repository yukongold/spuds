﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * @file Game_Stats.cs
 * @author Yukon
 * @date November 28, 2017
 * @brief Game stats collecting/display script
 * 
 * This script is used to collect stats from the player's gameplay. 
 * It displays the remaining health, shot accuracy, kill count, and score at the win/loss conditions
 */

public class Game_Stats : MonoBehaviour {
    private Player_Move player;
    public int health;
    public Text statsOutput;
    public int enemiesKilled;
    public int wavesCleared;
    public int shotsFired = 0;
    public int shotsHit = 0;
    private float shotAccuracy = 0;
    public int kills = 0;

	/**
     * Use this for initialization.
     */
	void Start () {
        Opacity(false);
	}

    /**
     * Update is called once per frame
     */
	void Update () {
        OutputStatsText();
        
	}
  
    /**
     * Produces the stats output to be printed after the game is over
     */
    void OutputStatsText()
    {
        string healthText = "Health: " + health + "\n";
        string shotsFiredText = "Shots fired: " + shotsFired + "\n";
        string shotsHitText = "Shots hit: " + shotsHit + "\n";
        string shotAccuracyText = "Shot Accuracy: " + ShotAccuracy().ToString("f3") + "\n";
        string killsText = "Kill count: " + kills + "\n";
        string score = "SCORE: " + Score() + "\n";
        statsOutput.text = healthText + shotAccuracyText + killsText + score;
    }

    /**
     * Calculates the general accuracy of shots fired by the player
     */
    float ShotAccuracy()
    {
        shotAccuracy = ((float)shotsHit / shotsFired);
        return shotAccuracy;
    }

    /**
     * Controls the opacity of the stats screen (statsOutput).
     */
    public void Opacity(bool opaque)
    {
        Color colorText = statsOutput.color;
        if (opaque)
        {
            colorText.a = 1;
        }
        else
        {
            colorText.a = 0;
        }
        statsOutput.color = colorText;
    }

    /**
     * Calculates the player's score based on waves cleared and enemies killed.
     */
    int Score()
    {
        int pointsWaves = wavesCleared * 1000;
        int pointsKills = kills * 10;
        return pointsWaves + pointsKills;
    }
}
