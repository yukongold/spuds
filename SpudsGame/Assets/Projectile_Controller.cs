﻿/**
 * @file Projectile_Controller.cs
 * @author Yukon
 * @brief Player projectile ontroller Script
 * 
 * The script controls specifics of projectiles fired by the player
 */
 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile_Controller : MonoBehaviour {

    public float xSpeed;
    public float ySpeed;

    public GameObject impactEffect;
	
	private FlyingFollower flyEnemy;
    private Game_Stats gameStats;

	/**
     * Use this for initialization.
     */
	void Start () {
        StartCoroutine(DestroyBullet());
		flyEnemy = GameObject.FindGameObjectWithTag("Enemy").GetComponent<FlyingFollower>();
        gameStats = GameObject.FindGameObjectWithTag("GameStats").GetComponent<Game_Stats>();
    }
	
	/**
	 * Update is called once per frame.
	 */
	void Update () {
        Vector2 position = transform.position;
        position.x += xSpeed;
        position.y += ySpeed;
        transform.position = position;
    }

    /*
     * Destroy projectile after certain time
     */
    IEnumerator DestroyBullet()
    {
        yield return new WaitForSeconds(4f);
        Destroy(gameObject);
    }

    /**
	 * @brief Triggers code upon projectile collision with another object with collider
	 *
	 * If projectile hits enemy, register damage on enemy and increment shots hit statistic.
	 * May be bugged, sometimes 1 hit registers as two triggers
	 */
    private void OnTriggerEnter2D(Collider2D collision) 
    {
        if(collision.gameObject.tag == "Enemy")
        {
            flyEnemy.Damage(1); 
            gameStats.shotsHit++; 
        }

        // Destroy particle effects after animation
        Destroy(Instantiate(impactEffect, transform.position, transform.rotation), 0.3f);

        // Destroy projectile on collision
        Destroy(gameObject);
    }
}
