﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * @file Time_Wave_Display.cs
 * @author Yukon
 * @date December 2, 2017
 * @brief Time and current wave display script
 * 
 * This script is used to display the game running time and the current enemy wave 
 * It currently prints them out while the game is running.
 */


public class Time_Wave_Display : MonoBehaviour {
    private Game_Stats gameStats;
    private Player_Move player;

    private float startTime;
    private string aliveTime;
    public float timeSinceStart;
    public Text timeWave;
    

	// Use this for initialization
	void Start () {
        startTime = Time.time;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Move>();
        gameStats = GameObject.FindGameObjectWithTag("GameStats").GetComponent<Game_Stats>();
	}
	
	// Update is called once per frame
	void Update () {
        OutputTimeWaveText();
	}

    /**
     * Produces the wave and time output to be printed during the game
     */
    void OutputTimeWaveText()
    {
        string timeText = "Time: " + Timer() + "\n";
        string waveText = "Wave: " + (gameStats.wavesCleared + 1) + "\n";
        timeWave.text = timeText + waveText;
    }

    /**
     * Increments the timer in real time
     */
    string Timer()
    {
        if (!player.dead)
        {
            timeSinceStart = Time.time - startTime;
            string minutes = ((int)timeSinceStart / 60).ToString();
            string seconds = (timeSinceStart % 60).ToString("f2");
            aliveTime = minutes + ":" + seconds;
        }
        return aliveTime;
    }
}
