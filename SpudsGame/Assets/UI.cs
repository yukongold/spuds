﻿/**
 * @file UI.cs
 * @author Jin
 * @brief Health UI script
 * 
 * The script controls the health UI for the player.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour {

	public Sprite[] heartSprites;
	public Image healthUI;
	private Player_Move player;
	
	
	/**
     * Use this for initialization.
     */
	void Start() {
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Move>();
	}
	
	/**
	 * Update is called once per frame.
	 * 
	 * Updates player health with UI elements
	 */
	void Update(){
		
		healthUI.sprite = heartSprites[player.currentHealth];
		
	}


}
