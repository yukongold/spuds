﻿/**
 * @file Player_Move.cs
 * @author Yukon
 * @date November 26, 2017
 * @brief Player controller Script
 * 
 * The script controls the movement, health, and projectiles fired for the player
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Move : MonoBehaviour {

    private Game_Stats gameStats;

    public int playerSpeed = 10;
    public bool facingRight = false;
    public int playerJumpPower = 1250;
    public float moveX;
    public bool isGrounded = true;
    public int jumpCount = 0;
    public int jumpLimit = 1;
    public bool canDropThrough = false;
    public BoxCollider2D playerCollider;
    public Rigidbody2D rb;
	public float damageTimeout = 0.75f;
	public bool canDamage = true;

	//Health Stats
	public int currentHealth;
	public int maxHealth = 3;
    public bool dead = false;
	
	public GameObject projectile;
	public float projectileSpeed;
	public float extension;             // extension of projectile emiiter from player body
	public float fireRate;
	public float nextFire;

	[SerializeField]
	private GameObject gameOverUI;


    /**
     * Use this for initialization.
     */
    void Start () {
		Time.timeScale = 1;
        gameStats = GameObject.FindGameObjectWithTag("GameStats").GetComponent<Game_Stats>();
        playerCollider = gameObject.GetComponent<Rigidbody2D>().GetComponent<BoxCollider2D>();
        rb = gameObject.GetComponent<Rigidbody2D>();
		currentHealth = maxHealth;
        gameStats.health = currentHealth;
    }
	
	/**
	 * Update is called once per frame.
	 * It calls PlayerMove(), Shoot(), and Death()
	 */
	void Update () {
        PlayerMove();
        Shoot();
		Death();
    }
	
	/**
	 * @brief Logic for damage application upon collision.
	 * 
	 * Checks if collided with object has "Enemy" tag and whether or not player is state of invulnerability.
	 * Calls damage and knockback if triggered.
	 */
	void OnTriggerEnter2D(Collider2D col) {
		if(col.CompareTag("Enemy")) {
			if (canDamage) {
				Damage(1);
				StartCoroutine(Knockback(0.02f, 1500, transform.position));
			}
		}
	}
	
	/**
	 * @brief Logic for damage. 
	 * 
	 * Subtracts damage taken from health, plays animation for being hit, and reflects changes on stats.
	 * Called whenever player health is impacted in the form of damage.
	 */
	public void Damage(int dmg) {
		currentHealth -= dmg;
		gameObject.GetComponent<Animation>().Play("Player_Flash");
		gameStats.health = currentHealth;
		StartCoroutine (damageWait(damageTimeout));
	}
	
	/**
	 * @brief Logic for damage invincibility after being hit. 
	 * 
	 * Introduces delay for next instance of damage.
	 * Called by Damage()
	 */
	public IEnumerator damageWait(float dmgTime) {
		canDamage = false;
		yield return new WaitForSeconds(dmgTime);  // wait for damage timeout
		canDamage = true;
	}

	/**
     * @brief Knockback functionality for player taking damage.
	 * 
	 * Called by OnTriggerEnter2D()
     */
	public IEnumerator Knockback(float knockDur, float knockPow, Vector3 knockDir) {
			
			float timer = 0;
			
			while (knockDur > timer) {
				
				timer += Time.deltaTime;
				
				if(facingRight) {
					rb.AddForce(new Vector3(knockDir.x * -800, knockDir.y + knockPow, transform.position.z));
				} else if (!facingRight) {
					rb.AddForce(new Vector3(knockDir.x * 800, knockDir.y + knockPow, transform.position.z));
				}
				
			}
		
		yield return 0;
	}
	
	/**
	 * @brief Logic to kill player upon health depletion. 
	 * 
	 * Activates Game Over Screen.
	 * Called when player health reaches 0.
	 */
	void Death() {
        if (currentHealth <= 0) {
		    Destroy(gameObject);
            gameOverUI.SetActive(true);
            gameStats.Opacity(true);
            dead = true;
			Time.timeScale = 0;
        }
	}

    /**
     * @brief Droppable platform
     * 
     * Handles dropping through eligible platforms.
     * Removes collider for player until it passes through the platform.
     */
    void DropThroughPlatform()
    {
        if (Input.GetKeyDown(KeyCode.S) && canDropThrough && isGrounded && !IsFalling(rb)) 
        {
            playerCollider.isTrigger = true;
        }
    }

    /**
     * @brief Trigger when exits 2D collider
     * 
     * Enables colliders for player after dropping through platform. Used with
     * #DropThroughPlatform and triggers after the player passes through the platform.
     */
    void OnTriggerExit2D(Collider2D coll)
    {
        playerCollider.isTrigger = false;
    }

	/**
	 * @brief Player Controller
	 * 
	 * Detects player key presses to move the player character from side to side and for jumping.
	 * Controls physics for movement.
	 */
    void PlayerMove()
    {
        //CONTROLS
        moveX = Input.GetAxisRaw("Horizontal");
        if (Input.GetButtonDown("Jump") && jumpCount < jumpLimit)
        {
            jumpCount++;
            Jump();
        }
        DropThroughPlatform();
        //ANIMATIONS
        //PLAYER DIRECTION
        if(moveX < 0.0f && facingRight == false)
        {
            FlipPlayer();
        }
        else if(moveX > 0.0f && facingRight == true)
        {
            FlipPlayer();
        }
        //PHYSICS
        rb.velocity = new Vector2(moveX * playerSpeed, rb.velocity.y);
		
    }

    /**
     * Adds an upward force for the character to jump.
     */
    void Jump()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, 0);
        GetComponent<Rigidbody2D>().AddForce(Vector2.up * playerJumpPower);
        isGrounded = false;
    }

    /**
     * Flips the player to face the direction it is going.
     */
    void FlipPlayer()
    {
        facingRight = !facingRight;
        Vector2 localScale = gameObject.transform.localScale;
        localScale.x *= -1;
        transform.localScale = localScale;
    }

    /**
     * Checks if the rigid body is in contact with the ground.
     * Also checks if player is on a platform that can be fallen through
     * Once per frame.
     */
    void OnCollisionStay2D(Collision2D col)
    {
        canDropThrough = false;
        if (col.gameObject.tag == "Ground" || col.gameObject.tag == "Drop_Through")
        {
            //Debug.Log("colliding " + col.gameObject.tag);
            isGrounded = true;
            jumpCount = 0;
            if(col.gameObject.tag == "Drop_Through")
            {
                canDropThrough = true;
            }
            else
            {
                canDropThrough = false;
            }

        }
    }

    /**
     * Checks if the rigidbody is falling
     * 
     * @return True or False
     */
    bool IsFalling(Rigidbody2D rb)
    {
        if(rb.velocity.y < -0.1)
        {
            return true;
        }
        return false;
    }

    /**
     * Detects held down arrow keys for shooting direction.
     * Used with ShootDirection to shoot projectiles.
     */
    void Shoot()
    {
        if (Time.time > nextFire)
        {
            if (ArrowUp() && ArrowRight())
            {
                ShootDirection(extension, extension, projectileSpeed, projectileSpeed);
            }
            else if (ArrowUp() && ArrowLeft())
            {
                ShootDirection(-extension, extension, -projectileSpeed, projectileSpeed);
            }
            else if (ArrowDown() && ArrowRight())
            {
                ShootDirection(extension, -extension, projectileSpeed, -projectileSpeed);
            }
            else if (ArrowDown() && ArrowLeft())
            {
                ShootDirection(-extension, -extension, -projectileSpeed, -projectileSpeed);
            }
            else if (ArrowRight())
            {
                ShootDirection(extension, 0, projectileSpeed, 0);
            }
            else if (ArrowLeft())
            {
                ShootDirection(-extension, 0, -projectileSpeed, 0);
            }
            else if (ArrowUp())
            {
                ShootDirection(0, extension, 0, projectileSpeed);
            }
            else if (ArrowDown())
            {
                ShootDirection(0, -extension, 0, -projectileSpeed);
            }
        }
    }


    /**
     * Sends projectiles in the directions specified by Shoot().
     * Takes projectile emitter extension direction and projectile speed directions as inputs.
     */
    void ShootDirection(float extensionX, float extensionY, float projectileSpeedX, float projectileSpeedY)
    {
        nextFire = Time.time + fireRate;
        Vector2 position = transform.position;
        position.x += extensionX;
        position.y += extensionY;
        GameObject go = (GameObject)Instantiate(projectile,
            position, Quaternion.identity);
        gameStats.shotsFired++;
        go.GetComponent<Projectile_Controller>().xSpeed = projectileSpeedX;
        go.GetComponent<Projectile_Controller>().ySpeed = projectileSpeedY;
    }

    /**
     * Detects if the up key is continually pushed.
     */
    bool ArrowUp()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            return true;
        }
        return false;
    }

	/**
     * Detects if the left key is continually pushed.
     */
    bool ArrowLeft()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            return true;
        }
        return false;
    }

	/**
     * Detects if the down key is continually pushed.
     */
    bool ArrowDown()
    {
        if (Input.GetKey(KeyCode.DownArrow))
        {
            return true;
        }
        return false;
    }

	/**
     * Detects if the right key is continually pushed.
     */
    bool ArrowRight()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            return true;
        }
        return false;
    }

}
