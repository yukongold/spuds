﻿/**
 * @file WaveSpawner.cs
 * @author Yukon
 * @brief Wave spawner script
 * 
 * This script controls wave spawner component.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawner : MonoBehaviour {

    private bool canSpawn = true; // debug, enable/disable enemy spawn
    public enum SpawnState {SPAWNING, WAITING, COUNTING};
    private Game_Stats gameStats;

	/**
     * Allows editing inside Unity
	 * Wave constructor
     */
    [System.Serializable]
    public class Wave {
        public string name; //name of wave
        public Transform enemy; //point at which enemy spawns
        public int count; //amount spawned
        public float rate; 
    }

    public Wave[] waves;
    private int nextWave = 0; //index of next wave

    public Transform[] spawnPoints;

    public float timeBetweenWaves = 4f;
    public float waveCountdown;

    private float searchCountdown = 1f; 

    private SpawnState state = SpawnState.COUNTING;

	[SerializeField]
	private GameObject winScreenUI;

	/**
     * Use this for initialization.
     */
    void Start()
    {
        gameStats = GameObject.FindGameObjectWithTag("GameStats").GetComponent<Game_Stats>();
        if (spawnPoints.Length == 0)
        {
            Debug.LogError("No spawn points referenced");
        }

        waveCountdown = timeBetweenWaves;
    }

	/**
	 * Update is called once per frame.
	 *
	 * Checks if enemies are able to spawn.
	 * Handles timer between waves.
	 */
    void Update()
    {
        if (canSpawn)
        {
            if (state == SpawnState.WAITING)
            {
                if (!EnemyIsAlive())
                {
                    WaveCompleted();
                }
                else
                {
                    return;
                }
            }

            if (waveCountdown <= 0) //Countdown wave and spawn
            {
                if (state != SpawnState.SPAWNING)
                {
                    StartCoroutine(SpawnWave(waves[nextWave]));
                    //Enumerates through all waves to be spawned using IEnum
                }
            }
            else
            {
                waveCountdown -= Time.deltaTime;
            }
        }
    }
	
	/**
	 * @brief Handles case when a wave is completed.
	 *
	 * Increments waves cleared and associated statistic
	 * Tracks amount of waves completed and handles when all waves are complete
	 * 
	 * On completion of all waves, the scene is stopped and the winScreenUI
	 * gameObject is played
	 */
    void WaveCompleted()
    {
        //Begin new round
        Debug.Log("Wave completed");
        gameStats.wavesCleared++;
        state = SpawnState.COUNTING;
        waveCountdown = timeBetweenWaves;

        if(nextWave + 1 > waves.Length - 1)
        {
            nextWave = 0;
			winScreenUI.SetActive (true);
            gameStats.Opacity(true);
			Time.timeScale = 0;
            Debug.Log("All waves complete, Looping");
        } else {
            nextWave++;
        }   
    }
	
	/**
	 * @brief Checks if any enemies exist in the current scene.
	 *
	 * Searches scene if any objects with tag "Enemy" exist.
	 * @return True or False
	 */
    bool EnemyIsAlive()
    {
        searchCountdown -= Time.deltaTime;
        if(searchCountdown <= 0f)
        {
            searchCountdown = 1f;
            if (GameObject.FindGameObjectWithTag("Enemy") == null) {
                return false;
            }
        }
        return true;
    } 
	
	/**
	 * Handles counter for rate of enemies spawned.
	 */
    IEnumerator SpawnWave (Wave _wave)
    {
        Debug.Log("New wave:" + _wave.name);
        state = SpawnState.SPAWNING; 
        
        for (int i = 0; i < _wave.count; i++)
        {
            SpawnEnemy(_wave.enemy);
            yield return new WaitForSeconds( 1f/_wave.rate); 
            //waits for 1 sec divided by rate of spawn
            //can also use _wave.delay for fixed delay
        }

        state = SpawnState.WAITING; 

        yield break;
    }

	/**
	 * @brief Function for spawning an enemy at a set of predetermined spawn points.
	 *
	 * Chooses a spawnpoint within the array of spawning points on the scene and instantiates an enemy.
	 */
    void SpawnEnemy (Transform _enemy)
    {
        Debug.Log("Spawning Enemy: " + _enemy.name);

        Transform _sp = spawnPoints[ Random.Range(0, spawnPoints.Length) ];
        Instantiate(_enemy, _sp.position, _sp.rotation); 
    }
}
