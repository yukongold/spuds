﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Main_Menu : MonoBehaviour {

	/**
	 * Play game button, loads scene of the next index
	 */ 
	public void PlayGame ()
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
	}

	/**
	 * Quit button, quits the application
	 */ 
	public void QuitGame ()
	{
		Debug.Log ("Quitting");
		Application.Quit ();
	}

}
