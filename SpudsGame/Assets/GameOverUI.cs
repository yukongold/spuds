﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/**
 * GameOverUI and button behavior
 */

public class GameOverUI : MonoBehaviour {

	/**
	 * @brief End screen button behavior
	 * 
	 * Loads starting scene when the player pushes the restart button
	 */
	public void Restart () 
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}
}
