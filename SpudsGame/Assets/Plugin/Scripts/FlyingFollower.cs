﻿/**
 * @file FlyingFollower.cs
 * @author Yukon
 * @brief Flying Enemy Script
 * 
 * This script controls the flying potato enemy.
 */
using UnityEngine;
using System.Collections;

public class FlyingFollower : MonoBehaviour {

	public Transform whoToFollow;
	public float velocity= 2f;
	public float distanceFromTargetX = 1.0f;
	public float distanceFromTargetY = 1.0f;
	
	//Health Stats
	public int currentHealth;
	public int maxHealth = 2;
	
	public bool willItTurn = false;
	
	private Transform thisTransform;
	
	private Player_Move player;
    private Game_Stats gameStats;

	private bool facingRight = true;
	private float distance;
	
	/**
     * Use this for initialization.
     */
	void  Start (){
		thisTransform = transform;
		currentHealth = maxHealth;
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player_Move>();
        gameStats = GameObject.FindGameObjectWithTag("GameStats").GetComponent<Game_Stats>();
	}

	/**
	 * Update is called once per frame.
	 * 
	 * Checks if current health is 0 to determine whether to call Death().
	 */
	void Update (){
		if (currentHealth <= 0) {
			Death();
		}
	}
	
	/**
	 * @brief Logic for damage. 
	 * 
	 * Subtracts damage taken from health.
	 * Called whenever enemy health is impacted in the form of damage from player.
	 */
	public void Damage(int dmg) {
		currentHealth -= dmg;
	}
	
	/**
	 * @brief Logic to kill enemy
	 * 
	 * Adds to Total Kill Amount game stat upon death.
	 */
	void Death() {
		Destroy(gameObject);
        gameStats.kills++;
	}
	
	//You only have to change the "PlayerController" and the "facingRight" parts if it's turning.

	void  LateUpdate (){

		float positionX;
		float positionY;

		if (willItTurn) {

			if (GameObject.Find(whoToFollow.name).GetComponent<Player_Move>().facingRight) {
				positionX = Mathf.Lerp (thisTransform.position.x, GameObject.FindGameObjectWithTag("Player").transform.position.x - distanceFromTargetX, Time.deltaTime * velocity);
			} else {
				positionX = Mathf.Lerp (thisTransform.position.x, GameObject.FindGameObjectWithTag("Player").transform.position.x + distanceFromTargetX, Time.deltaTime * velocity);
			}
			positionY = Mathf.Lerp (thisTransform.position.y, GameObject.FindGameObjectWithTag("Player").transform.position.y + distanceFromTargetY, Time.deltaTime * velocity);

			thisTransform.position = new Vector3 (positionX, positionY, 0);

			if (GameObject.Find (whoToFollow.name).GetComponent<Player_Move> ().facingRight == true && !facingRight) {
				Flip ();
			} else if (GameObject.Find (whoToFollow.name).GetComponent<Player_Move> ().facingRight == false && facingRight) {
				Flip ();
			}
		} else if (!willItTurn) {

			positionX = Mathf.Lerp (thisTransform.position.x, GameObject.FindGameObjectWithTag("Player").transform.position.x - distanceFromTargetX, Time.deltaTime * velocity);
			positionY = Mathf.Lerp (thisTransform.position.y, GameObject.FindGameObjectWithTag("Player").transform.position.y + distanceFromTargetY, Time.deltaTime * velocity);
			thisTransform.position = new Vector3 (positionX, positionY, 0);
						
		}
	}
	
	void Flip(){
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

}