
# Spuds
Spuds is a 2D shoot 'em up game built using Unity. This game is linear and combat-centric, focusing on defeating bosses.

## Repository Overview
Folder | Description
--- | ---
Documents | Contains all necessary documents for scrum.
SpudsGame | Base folder for the Unity game.

## Documents Folder
This folder contains all necessary documents for scrum.
The respective sprint folders contain specific sprint only documents.
Any documents outside of those folders are generic release documents.

## SpudsGame Folder
This folder contains all necessary files to run the Unity game.
Assets contain the 

## Important Links
Spuds game:
https://yukongold.bitbucket.io/

Doxygen documentation:
https://auyen.bitbucket.io/

Spuds game (with broken boss):
https://auyen.github.io/
