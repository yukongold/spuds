/**
 * @file main.c
 * @author Yukon
 * @date November 26, 2017
 * @brief This is the main file of the project
 *
 * \mainpage Description
 * Spuds is a 2D arena based Shoot 'Em Up platformer where
 * the player clears waves by shooting at enemies and then a boss
 */